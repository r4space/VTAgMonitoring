#Bar data pipeline
Primarily flown over three distinct fields (geographic boundaries) in tandem with field data collection and with Sattelite Overpasses (when appropriate). 

Imagery from 3 camera sensors: 
1. RGB camera
2. NIR camera
3. SEQUOIA multispec camera

outputs: raw jpegs that we then stitch together using Pix4D (program) and end with a moisaiced geotiff. 


Atmospheric from 2 sensors:
4. iMET-IQ pressure/temp/humidity sensor (+gps + time) 
5. CO2 Sensor 

outputs: csv files - could be linked or not to drone autopilot. (e.g. either separate csv files not related at all to drone specific data OR could be collected all in a raspberry pi and exported as a csv along with relevant drone autopilot data lat/long, estimated wind speed, etc).


Mission Planning / Drone Autopilot Files:

6. Drone Data Logbook (flight plan and mission data capture) **I actually don't know if this is useful yet but it seems like it and is something that Spatial Analysis Lab is trying to switch to using this very heavily for ex silico data. 
7. Drone autopilot files (from Iris - Pixhawk, and SenseFly - eBee files I don't think I can actually open the eBee files, but can still send them along.) 


Which I am merging / comparing / integrating between:
8. Landsat 8 Satellite Imagery (geotiff)
9. Photoacouster Gas Analyzer (+ on ground sensors) (csv, human collected)
10. Decagon Atmospheric Weather Station Sensors (csv, continuous data)

