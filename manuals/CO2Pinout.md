# K30 sensor pinout
	* Right to Left with board orientated such that connectivity pins are in the bottom right hand corner and the rightmost pin is the last pin in the ro
		- Top Row: Serial (RIGHT to LEFT)
		  	1. NC
			2. GND
			3. 6V
			4. RXD
			5. TXD
		- Bottom Row: I2C (RIGHT to LEFT)
			1. NC
			2. GND
			3. 6V
			4. SCL
			5. SDA
