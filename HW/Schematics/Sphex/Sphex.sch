EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+5V #PWR?
U 1 1 5B29BD07
P 3050 800
F 0 "#PWR?" H 3050 650 50  0001 C CNN
F 1 "+5V" H 3065 973 50  0000 C CNN
F 2 "" H 3050 800 50  0001 C CNN
F 3 "" H 3050 800 50  0001 C CNN
	1    3050 800 
	1    0    0    -1  
$EndComp
$Comp
L power:+6V #PWR?
U 1 1 5B29BD89
P 1900 800
F 0 "#PWR?" H 1900 650 50  0001 C CNN
F 1 "+6V" H 1915 973 50  0000 C CNN
F 2 "" H 1900 800 50  0001 C CNN
F 3 "" H 1900 800 50  0001 C CNN
	1    1900 800 
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5B29BFA1
P 850 800
F 0 "#PWR?" H 850 650 50  0001 C CNN
F 1 "VCC" H 867 973 50  0001 C CNN
F 2 "" H 850 800 50  0001 C CNN
F 3 "" H 850 800 50  0001 C CNN
	1    850  800 
	1    0    0    -1  
$EndComp
Text Label 700  700  0    50   ~ 0
4SLiPo
$Comp
L Regulator_Linear:KA378R05 U?
U 1 1 5B29CBDF
P 2600 1400
F 0 "U?" H 2600 1650 50  0000 C CNN
F 1 "KA378R05" H 2600 1651 50  0001 C CNN
F 2 "Package_TO_SOT_THT:TO-220-4_Vertical" H 2600 1700 50  0001 C CIN
F 3 "http://www.fairchildsemi.com/ds/KA/KA378R05.pdf" H 2600 1500 50  0001 C CNN
	1    2600 1400
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:KA378R05 U?
U 1 1 5B29CB4C
P 1500 1400
F 0 "U?" H 1500 1650 50  0000 C CNN
F 1 "KA378R05" H 1500 1651 50  0001 C CNN
F 2 "Package_TO_SOT_THT:TO-220-4_Vertical" H 1500 1700 50  0001 C CIN
F 3 "http://www.fairchildsemi.com/ds/KA/KA378R05.pdf" H 1500 1500 50  0001 C CNN
	1    1500 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 1300 1100 1300
Wire Wire Line
	1100 1300 1100 1050
Wire Wire Line
	1100 1050 2300 1050
Wire Wire Line
	2300 1050 2300 1300
Wire Wire Line
	1100 1050 850  1050
Connection ~ 1100 1050
Wire Wire Line
	850  1050 850  800 
Wire Wire Line
	1900 800  1900 1300
Wire Wire Line
	1900 1300 1800 1300
Wire Wire Line
	3050 800  3050 1300
Wire Wire Line
	3050 1300 2900 1300
Wire Wire Line
	800  1700 1500 1700
Wire Wire Line
	1500 1700 2600 1700
Connection ~ 1500 1700
$Comp
L Display_Character:EA_T123X-I2C K30_CO2Meter
U 1 1 5B29F076
P 1600 3000
F 0 "K30_CO2Meter" H 2030 3000 50  0000 L CNN
F 1 "EA_T123X-I2C" H 2030 3045 50  0001 L CNN
F 2 "Display:EA_T123X-I2C" H 1600 2400 50  0001 C CNN
F 3 "http://www.lcd-module.de/pdf/doma/t123-i2c.pdf" H 1600 2500 50  0001 C CNN
	1    1600 3000
	-1   0    0    1   
$EndComp
$Comp
L Display_Character:EA_T123X-I2C K30_CO2Meter
U 1 1 5B29F1B0
P 1600 4250
F 0 "K30_CO2Meter" H 2030 4250 50  0000 L CNN
F 1 "EA_T123X-I2C" H 2029 4205 50  0001 L CNN
F 2 "Display:EA_T123X-I2C" H 1600 3650 50  0001 C CNN
F 3 "http://www.lcd-module.de/pdf/doma/t123-i2c.pdf" H 1600 3750 50  0001 C CNN
	1    1600 4250
	-1   0    0    1   
$EndComp
$Comp
L Display_Character:EA_T123X-I2C K30_CO2Meter
U 1 1 5B2A04A3
P 1600 5500
F 0 "K30_CO2Meter" H 2030 5500 50  0000 L CIN
F 1 "EA_T123X-I2C" H 2029 5455 50  0001 L CNN
F 2 "Display:EA_T123X-I2C" H 1600 4900 50  0001 C CNN
F 3 "http://www.lcd-module.de/pdf/doma/t123-i2c.pdf" H 1600 5000 50  0001 C CNN
	1    1600 5500
	-1   0    0    1   
$EndComp
$Comp
L Display_Character:EA_T123X-I2C K30_CO2Meter
U 1 1 5B2A0F8D
P 1600 6700
F 0 "K30_CO2Meter" H 2030 6700 50  0000 L CNN
F 1 "EA_T123X-I2C" H 2029 6655 50  0001 L CNN
F 2 "Display:EA_T123X-I2C" H 1600 6100 50  0001 C CNN
F 3 "http://www.lcd-module.de/pdf/doma/t123-i2c.pdf" H 1600 6200 50  0001 C CNN
	1    1600 6700
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B2A189B
P 800 7500
F 0 "#PWR?" H 800 7250 50  0001 C CNN
F 1 "GND" H 805 7327 50  0000 C CNN
F 2 "" H 800 7500 50  0001 C CNN
F 3 "" H 800 7500 50  0001 C CNN
	1    800  7500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J?
U 1 1 5B2A1DB1
P 9700 3650
F 0 "J?" H 9750 4767 50  0001 C CNN
F 1 "Pi3 GPIO header" H 9750 4676 50  0000 L BIN
F 2 "" H 9700 3650 50  0001 C CNN
F 3 "~" H 9700 3650 50  0001 C CNN
	1    9700 3650
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5B2A1F89
P 9500 2250
F 0 "#PWR?" H 9500 2100 50  0001 C CNN
F 1 "+3.3V" H 9515 2423 50  0000 C CNN
F 2 "" H 9500 2250 50  0001 C CNN
F 3 "" H 9500 2250 50  0001 C CNN
	1    9500 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 2250 9500 2400
$Comp
L Device:R_US 1k8
U 1 1 5B2A25B6
P 9000 2550
F 0 "1k8" H 9068 2550 50  0000 L CNN
F 1 "R_US" H 9068 2505 50  0001 L CNN
F 2 "" V 9040 2540 50  0001 C CNN
F 3 "~" H 9000 2550 50  0001 C CNN
	1    9000 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US 1k8
U 1 1 5B2A2638
P 8700 2550
F 0 "1k8" H 8768 2550 50  0000 L CNN
F 1 "R_US" H 8768 2505 50  0001 L CNN
F 2 "" V 8740 2540 50  0001 C CNN
F 3 "~" H 8700 2550 50  0001 C CNN
	1    8700 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 2400 9000 2400
Wire Wire Line
	9000 2400 9500 2400
Connection ~ 9000 2400
Connection ~ 9500 2400
Wire Wire Line
	9500 2400 9500 2750
Wire Wire Line
	9000 2700 9000 2850
Wire Wire Line
	9000 2850 9500 2850
Wire Wire Line
	9500 2950 8700 2950
Wire Wire Line
	8700 2950 8700 2700
Text Label 9100 2850 0    50   ~ 0
SDA1
Text Label 8750 2950 0    50   ~ 0
SCL1
Text Notes 8650 1700 0    50   ~ 0
Pi3 Board powered by micro usb port\n
Wire Notes Line
	8450 1450 8450 4800
Wire Notes Line
	8450 4800 10700 4800
Wire Notes Line
	10700 4800 10700 1450
Wire Notes Line
	10700 1450 8450 1450
Wire Wire Line
	3050 1300 8550 1300
Connection ~ 3050 1300
Wire Wire Line
	8550 1300 8550 1450
Wire Wire Line
	8700 4800 8700 6200
Wire Wire Line
	800  1700 800  3700
Connection ~ 800  7500
Wire Wire Line
	1700 7200 1700 7500
Connection ~ 1700 7500
Wire Wire Line
	1700 7500 800  7500
Wire Wire Line
	1700 6000 1700 6150
Wire Wire Line
	1700 6150 800  6150
Connection ~ 800  6150
Wire Wire Line
	800  6150 800  7500
Wire Wire Line
	1700 4750 1700 4850
Wire Wire Line
	1700 4850 800  4850
Connection ~ 800  4850
Wire Wire Line
	800  4850 800  6150
Wire Wire Line
	1700 3500 1700 3700
Wire Wire Line
	1700 3700 800  3700
Connection ~ 800  3700
Wire Wire Line
	800  3700 800  4850
Wire Wire Line
	9000 2850 3650 2850
Wire Wire Line
	2100 2850 2100 3200
Connection ~ 9000 2850
Wire Wire Line
	2100 4450 2750 4450
Wire Wire Line
	2750 4450 2750 2850
Connection ~ 2750 2850
Wire Wire Line
	2750 2850 2100 2850
Wire Wire Line
	2100 5700 3200 5700
Wire Wire Line
	3200 5700 3200 2850
Connection ~ 3200 2850
Wire Wire Line
	3200 2850 2750 2850
Wire Wire Line
	2100 6900 3650 6900
Wire Wire Line
	3650 6900 3650 2850
Connection ~ 3650 2850
Wire Wire Line
	3650 2850 3200 2850
Wire Wire Line
	8700 3300 8700 2950
Connection ~ 8700 2950
Wire Wire Line
	2100 5800 3300 5800
Wire Wire Line
	3300 5800 3300 3300
Connection ~ 3300 3300
Wire Wire Line
	3300 3300 3750 3300
Wire Wire Line
	2100 7000 3750 7000
Wire Wire Line
	3750 7000 3750 3300
Connection ~ 3750 3300
Wire Wire Line
	3750 3300 8700 3300
Wire Wire Line
	2100 3300 2850 3300
Wire Wire Line
	2100 4550 2850 4550
Wire Wire Line
	2850 4550 2850 3300
Connection ~ 2850 3300
Wire Wire Line
	2850 3300 3300 3300
Wire Wire Line
	6850 7500 6850 6200
Wire Wire Line
	6850 6200 8700 6200
Wire Wire Line
	6850 7500 1700 7500
Wire Wire Line
	1900 1300 1900 2450
Wire Wire Line
	1900 2450 900  2450
Wire Wire Line
	900  2450 900  3500
Wire Wire Line
	900  7200 1600 7200
Connection ~ 1900 1300
Wire Wire Line
	1600 6000 900  6000
Connection ~ 900  6000
Wire Wire Line
	900  6000 900  7200
Wire Wire Line
	1600 4750 900  4750
Connection ~ 900  4750
Wire Wire Line
	900  4750 900  6000
Wire Wire Line
	1600 3500 900  3500
Connection ~ 900  3500
Wire Wire Line
	900  3500 900  4750
$EndSCHEMATC
