import subprocess

def find_devices():
    #TODO convert prints to logging
    # Ids of USB ports found on a Pi3B+  possible differes on other devices
    ids = {"2": "A", "4": "B", "3": "C", "5": "D"}
    portID = "X"
    K30_productID = "ea60"
    Imet_productID = "6015"
    # TODO get actual Pixhawk product ID and add to search
    Pixhawk_productID = "BEEF"
    devices = {"k30s": [], "imets": [], "pixhawks": []}

    # Find all serial usb devices
    p = subprocess.Popen("ls /dev/ttyUSB*", stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    p_status = p.wait()
    ports = str(output).split("\\n")[:-1]
    ports[0] = ports[0][2:]

    if p_status == 0:
        # Search for each product id
        for d in ports:
            p = subprocess.Popen('udevadm info -a  --name={}'.format(d), stdout=subprocess.PIPE, shell=True)
            (output, err) = p.communicate()
            p_status = p.wait()
            output=str(output)
    
            if p_status == 0:
                if 'ATTRS{{idProduct}}=="{}"'.format(K30_productID) in output:
                    p = subprocess.Popen('udevadm info -a  --name={} | grep \'KERNELS=="1.\''.format(d),
                                         stdout=subprocess.PIPE, shell=True)
                    (output, err) = p.communicate()
                    p.wait()

                    # Pulls kernel identification of usb port from response
                    try:
                        portID = ids[str(output).split("\\n")[1][-2:-1]]
                        devices["k30s"].append((d, portID))
                    except KeyError as e:
                        print ("KeyError raised: {}".format(str(e)))
                    except IndexError as e:
                        print ("IndexError raised: {}".format(str(e)))


                elif 'ATTRS{{idProduct}}=="{}"'.format(Imet_productID) in output:
                    p = subprocess.Popen('udevadm info -a  --name={} | grep \'KERNELS=="1.\''.format(d),
                                         stdout=subprocess.PIPE, shell=True)
                    (output, err) = p.communicate()
                    p.wait()

                    # Pulls kernel identification of usb port from response
                    try:
                        portID = ids[str(output).split("\\n")[1][-2:-1]]
                        devices["imets"].append((d, portID))
                    except KeyError as e:
                        print ("KeyError raised: {}".format(str(e)))
                    except IndexError as e:
                        print ("IndexError raised: {}".format(str(e)))

                elif 'ATTRS{{idProduct}}=="{}"'.format(Pixhawk_productID) in output:
                    p = subprocess.Popen('udevadm info -a  --name={} | grep \'KERNELS=="1.\''.format(d),
                                         stdout=subprocess.PIPE, shell=True)
                    (output, err) = p.communicate()
                    p.wait()

                    # Pulls kernel identification of usb port from response
                    try:
                        portID = ids[str(output).split("\\n")[1][-2:-1]]
                        devices["pixhawks"].append((d, portID))
                    except KeyError as e:
                        print ("KeyError raised: {}".format(str(e)))
                    except IndexError as e:
                        print ("IndexError raised: {}".format(str(e)))

            else:
                print("Error, couldn't get udev information about ports")
                return -1
        return 0, devices
    else:
        print("Error: couldn't access prots, most likely this is a permissions issue or no sensors are plugged in")
        return -1

def main():
    realdevices = find_devices()
    print(realdevices)


if __name__ == "__main__":
    main()
