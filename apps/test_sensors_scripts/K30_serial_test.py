import time, datetime, serial, sys
import serial.tools.list_ports as port
def read_ppm(socket):
    """ Read data from a CO2 meter and return both the scaled result in ppm and a time stamp from when the reading was receieved
        stream = socket
        returns tuple of strings: time stamp, CO2 ppm value
    """
    socket.write("\xFE\x44\x00\x08\x02\x9F\x25")
    time.sleep(.5)
    resp = socket.read(7)
    try:
        high = ord(resp[3])
        low = ord(resp[4])
        co2 = (high * 256) + low
    except IndexError as e:
        e = sys.exc_info()
        print("Unexpected errors reading from socket{0} \n Error: {1}".format(socket, e))
        error="Unexpected errors reading from socket{0} \n Error: {1}".format(socket, e)
        return error
    return str(co2)

def serial_k30s():
    """
    Finds available k30 serial ports and determines which device is attached to which /dev/ path
    :return:
    A dictionary of devices labled as" K30<number starting from 0>
    A list of the ports for logging purposes
    """
    device_dict = {}
    k30s = 0
    portlist = list(port.comports())
    for p in portlist:
        sp = str(p)
        if "CP2102" in sp:
            path = sp.split('-')[0]
            device_dict["K30" + str(k30s)] = path[:-1]

            k30s = k30s + 1
        else:
            pass

    return device_dict, portlist

def kopen_ports(devices):
    """ Tries to open as many K30 device serial ports as there are
        Returns: a list of serial handles and a list of log entries to be written
    """
    k30_sockets = []
    log = []
    for d in range(len(devices)):
        port = str(devices["K30" + str(d)])
        try:
            ser = serial.Serial(port, baudrate=9600, timeout=.5)
            a=read_ppm(ser)
            time.sleep(1)
            k30_sockets.append(ser)
        except serial.SerialException as e:
            log.append("\nFailed to open k30 on port {}".format(devices["K30" + str(d)]))
            print("\nFailed to open K30 on port {}".format(devices["K30" + str(d)]))
            pass
        log.append("\nSuccesfully opened K30 on port {}".format(devices["K30" + str(d)]))
    return k30_sockets, log


(k30_dict, k30_ports) = serial_k30s()
k30_sockets,log=kopen_ports(k30_dict)
for k,p in zip(k30_sockets,k30_ports):
    values = read_ppm(k)
    print("Read port:"+str(p)+"  "+values)
    k.close()

