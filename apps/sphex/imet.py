import serial, time, argparse, signal, sys
"""
Imet Serial port specs:
Baud: 57600
Parity: None
Data Bits: 8 bits
Stop Bits: 1 bit 
Hardware Flow Control: None
Parameter positions:
XQ, Pressure, Temperature, Humidity, Date, Time, Latitude x 1000000, Longitude x 1000000, Altitude x1000, Sat Count

To test connection: 
$ python test_imet.py <path to imet port in /dev>
or
$ miniterm.py /dev/ttyUSB0  57600 --parity=N

"""

def openports(devices):
    imets=[]
    log=[]
    for d in range(len(devices)-1): #Create list of imet open ports
        port=str(devices["Imet"+str(d)])
        try:
            ser = serial.Serial(port, baudrate=57600, parity=serial.PARITY_NONE, bytesize=serial.EIGHTBITS,stopbits=serial.STOPBITS_ONE,timeout=3.0,xonxoff=False)
            imets.append(ser)
    
        except serial.SerialException as e:
            log.append("\nFailed to open imet on port {}".format(devices["Imet"+str(d)]))
            print("\nFailed to open imet on port {}".format(devices["Imet"+str(d)]))
            pass
        log.append("\nSuccesfully opened imet on port {}".format(devices["Imet"+str(d)]))
    return imets,log

def get_imet_time(imet_device):
    l = imet_device.readline()
    return l.split(',')[5:7]
