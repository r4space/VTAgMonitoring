#!/usr/bin/python  Application Note AN169: Raspberry pi3 to K33 via i2c
# -*- coding: utf-8 -*-

#      Operates K33-BLG, ELG and ICB
#      by Marv Kausch@CO2Meter.com 3/6/2017
#
import sys
import time
import pigpio

#        #
#         #      IMPORTANT!  Do the following:
############        After powering up RPi, from  Terminal window enter sudo pigpiod  
#         #			   This starts the pigpio daemon
#        #				

pi = pigpio.pi()
 
k33_addr = 0x68 # k33 co2 device address
sda = 2         # Raspberry pin #27
scl = 3         # Raspberry pin #17
baud=20000 #100000   at 20000, generates a 1 msec wakeup pulse
counter = 0
Debug = False

# bb_i2c_open(SDA, SCL, baud)
# This function selects a pair of GPIO for bit banging I2C at a specified baud rate. 
# Bit banging I2C allows for certain operations which are not possible with the standard Broadcom/RPi I2C driver.
# More details at http://abyz.co.uk/rpi/pigpio/python.html#pigpio.pi

print'      Application Note AN169: Raspberry pi3 to K33 via i2c'

def Wakeup():
    try:
        handle = pi.bb_i2c_open(sda,scl,baud)   # initialize the i2c bus

# bb_i2c_zip(SDA, data)
# This function executes a sequence of bit banged I2C operations. 
# The operations to be performed are specified by the contents of data which contains 

# the concatenated command codes and associated data

        dummy=pi.bb_i2c_zip(sda,[4,0x00,2,6,1,3,0]) # make a read on 0x00 address to wakeup sensor
        #print ("dummy = ",dummy)
        pi.bb_i2c_close(sda) #close i2c bus
    except pigpio.error:
        print 'Comunication error found  '
        pi.bb_i2c_close(sda)

def K33_query(data=[],count=0):  #send a query with data byte and receive number count bytes as return
    global b
    try:
        handle = pi.bb_i2c_open(sda,scl,baud) # execute a pi.bb_i2c initialization
        b = pi.bb_i2c_zip(sda,[4,k33_addr,2,7,len(data)]+data+[3,0])
        time.sleep(0.02)
        b = pi.bb_i2c_zip(sda,[4,k33_addr,2,6,count,3,0])
        pi.bb_i2c_close(sda)
    except pigpio.error:
        pi.bb_i2c_close(sda)
        print 'Read error - connection refused'
        b = ('',)
    return b


def start_measurement():
	global b
	b = K33_query([0x11,0x00,0x60,0x35,0XA6],1) # send ONE BYTE 0x35 to RAM address 0x60 '1' is # bytes to read back
	b = K33_query([0x21,0x00,0x1d,0x3e],1)# Read sensor RAM location 0x1d
	wait_secs = 0	
	while b[1]!='!':
		time.sleep(1)
		wait_secs +=1
		print wait_secs, ' seconds waiting  '
		if (wait_secs >= 10):
			print wait_secs, ' *** Sensor not responding'
			
		b = K33_query([0x21,0x00,0x1d,0x3e],1)# Read sensor RAM location 0x1d
	print ' '
		
  
def Read_Co2():
    global counter

    counter += 1
    print str(counter) + '\tRead_Co2  =', 
    b = K33_query([0x22,0x00,0x08,0x2A],4) # send the Query to K33 device and attend the 4 bytes to read
    if len(b) == 2: # check if the respond is a tuple with two arguments 
        values = b[1] # assign the arraybyte to variable values
        if len(values) == 4: # check if the values are 4
            if values[1] & 0b10000000 == 128: # it is negative value
                co2_value = (((values[1]*256)+values[2])-65536)
            else:
                co2_value = (values[1]*256)+values[2]
            if (values[0] == 0x21): # check if the request to client is complete
                if ((values[0]+values[1]+values[2]) & 0x00FF) == values[3]: #check the Checksum of reading byte
                    #print('{:6.2f}'.format(co2_value/1000.0)+'%')
                    #print '   ',(co2_value*10)
                    print '   ',(co2_value)
                else:
                    print 'Checksum Error'
            elif (values[0] == 0x20):
                print 'Request to client incomplete'

def Read_RH():
    print '\tRead_RH   =',
    b = K33_query([0x22,0x00,0x14,0x36],4)
    if len(b) == 2:
        values = b[1]
        if len(values) == 4:
            if values[1] & 0b10000000 == 128: # it is negative value
                RH_value = (((values[1]*256)+values[2])-65536)
            else:
                RH_value = (values[1]*256)+values[2]            
            if (values[0] == 0x21): # check if request to client is complete
                if ((values[0]+values[1]+values[2]) & 0x00FF) == values[3]: #check the Checksum 
                    pass
                    print('{:6.1f}'.format(RH_value/100.0)+'%')
                else:
                    print 'Checksum Error'
            elif (values[0] == 0x20):
                print 'Request to client incomplete'
            # Debug Window 

def Read_Temp():
    print '\tRead_Temp =',
    b = K33_query([0x22,0x00,0x12,0x34],4) # 0x22 is COMMAND #2:  READ RAM
    if len(b) == 2:

        values = b[1]
        if len(values) == 4:
            if values[1] & 0b10000000 == 128: # it is negative value
                Temp_value = (((values[1]*256)+values[2])-65536)
            else:
                Temp_value = (values[1]*256)+values[2]
            if (values[0] == 0x21): # check if request to client is complete
                if ((values[0]+values[1]+values[2]) & 0x00FF) == values[3]: #check the Checksum
                    pass
                    print('{:6.1f}'.format(Temp_value/100.0)+' °C')                    
                else:
                    print 'Checksum Error'
            elif (values[0] == 0x20):
                print 'Request to client incomplete'
                                          
#_____________________### main loop ###__________________________


while True:
    Wakeup()
    time.sleep(1)
    start_measurement()
    time.sleep(1)
    Read_Co2()
    time.sleep(1)
    Read_RH()
    Read_Temp()
    print ' '

