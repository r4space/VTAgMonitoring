import os, sys, datetime
import serial.tools.list_ports as port
"""File of system administrative funtions"""
##todo update to subprocess instead of os

def stop_files(files,msg):
    files[0].write(msg)
    sys.stdout.flush()
    files[0].close()
    files[1].close()

def stop_drone(v):
    v.close()

def stop_imets(imets):
    for i in imets:
        i.close()



def mk_ND(new_dir):
    """
    Make a new directory for this flight's data store
    """
    dt = str(datetime.datetime.today())[0:10]

    try:
        a=os.listdir(new_dir)
        num=len(a)
        ND=new_dir+dt+"Flight"+str(num+1).zfill(3)
        os.makedirs(ND)
    except OSError:
        pass

    return ND+"/"


