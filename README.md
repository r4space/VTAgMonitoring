Repository to maintain digital components of VTAgMonitoring custom built sensor systems

There are multiple applications in use for this project see the local subcomponent README for application specific use.

This repo is structured as:
* apps: executable applications
* manuals: sensor and similar manufacturerer documentation
* taranis: models for VTAg ardupilot drones: sphex and kestrel
* HW: Hawrdware relevant files and CAD models

## apps:
* shongololo: a python module for managing a pi based data capture system on the ground.  
    - Status: functional
* sphex:  a python module for managing a pi based data capture system onboard a ardupilot bsaed drone.  This is very similar to shongololo but caters for ardupilot metadata

    - Status: still under develeopment  
* test_sensors_scripts: stand-alone simple scripts for testing sensor operation
    - Status: functional
* sensor-logger: Python flask based web-frontend which can serve as the operational interface to sphex and shongololo
    - Status: functional with shongololo, still under development with sphex
## Documentation
* Manufacturer sensor manuals and documentation is available in ./manuals/
* application docs are in respective ddirectories
* Pi login details are:
    - user: uvm
    - password: VTAgMonitoring
    - Wifi SSID: VTAgPi< number corrosponding with the specific pi in use as follows>
        1. Ground station 1
        2. Ground station 2
        3. Ground station 3
        4. Ground station 4
        5. Ground station 5
        6. Hexacopter known as sphex
    - Wifi Password: VTAgMonitoring
